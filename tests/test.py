def add(n1, n2):
  return n1 + n2

class TestMyClass(unittest.TestCase):
  def test_add(self):
    self.assertTrue(add(1,2), 3)
    self.assertTrue(add(2,2), 4)
