try:
    f = open('myfile.txt')
    f.close()
except IOError:
    print('File is not accessible')
print('File is accessible')


def is_accessible(path, mode='r'):
    """
    Check if the file or directory at `path` can
    be accessed by the program using `mode` open flags.
    """
    try:
        f = open(path, mode)
        f.close()
    except IOError:
        return False
    return True
