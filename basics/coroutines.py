import asyncio as io

async def slow_operation(n):
  await io.sleep(1)
  print("Slow operation {} complete".format(n))
  
async def main():
  await io.wait([
    slow_operation(1),
    slow_operation(2),
    slow_operation(3),
  ])
  
loop = io.get_event_loop()
loop.run_until_complete(main())

