def pain():
  print("</''''''\>")
  
def tomates():
  print("#tomates")
  
def salade():
  print("~salade~")
  
def pain_bas():
  print("</______\>")
  
def sandwish(nourriture="--jambon--"):
  pain_haut()
  tomates()
  print(nouriture)
  salade()
  pain_bas()

  
# with decorators
def pain(func):
  def wrapper():
    print("</''''''\>")
    func()
    print("</______\>")
  return wrapper

def ingredients(func):
  def wrapper():
    print("#tomates")
    func()
    print("~salade~")
  return wrapper

@pain
@ingredients
def sandwish(nourriture="--jambon--"):
  print(nourriture)
