# Import packages
from urllib.request import urlretrieve
import matplotlib.pyplot as plt
import pandas as pd

# Assign url of file: url
url = 'https://www.data.gouv.fr/s/resources/liste-des-unites-et-etablissements-de-la-croix-rouge-francaise-csv/20150928-134340/structures.csv'

# Read file into a DataFrame: df
df = pd.read_csv(url, sep=';')

# Save file locally
urlretrieve(url, 'structures.csv')

# Print the head of the DataFrame
print(df.head())

# Plot first column of df
pd.DataFrame.hist(df.ix[:, 0:1])
plt.xlabel('structures')
plt.ylabel('nombres')
plt.show()
