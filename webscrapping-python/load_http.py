# Import packages
import ssl

from urllib.request import urlopen
from urllib.request import Request

import requests

# Specify the url
url = "https://delitamakanda.pythonanywhere.com"

# This packages the request: request
request = Request(url)

# Sends the request and catches the response: response
context = ssl._create_unverified_context()
response = urlopen(request, context=context)

# Packages the request, send the request and catch the response: r
r = requests.get(url, verify=False)

# Extract the response: text
text = r.text

# Extract the response: html
html = response.read()

# Print the datatype of response
print(type(response))

# Print the html
# print(html)

# Print the html
print(text)

# Be polite and close the response!
response.close()
