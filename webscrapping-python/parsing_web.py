# Import packages
import requests
from bs4 import BeautifulSoup

# Specify url: url
url = 'http://www.lemonde.fr/'

# Package the request, send the request and catch the response: r
r = requests.get(url)

# Extracts the response as html: html_doc
html_doc = r.text

# Create a BeautifulSoup object from the HTML: soup
soup = BeautifulSoup(html_doc, "html.parser")

# Get the title
guido_title = soup.title

print(guido_title)

guido_text = soup.get_text()

# Find all 'a' tags (which define hyperlinks): a_tags
a_tags = soup.find_all('a')

# Prettify the BeautifulSoup object: pretty_soup
# pretty_soup = soup.prettify()

# Print the response
# print(pretty_soup)

# print(guido_text)

# Print the URLs to the shell
for link in a_tags:
    print(link.get('href'))
